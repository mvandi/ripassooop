package org.bitbucket.mvandi.wage_calculator.util;

import java.util.function.Function;
import java.util.function.Supplier;

public class ExceptionHelper extends NoInstancesObject {

    public static NotImplementedExeption TODO(final String methodName) {
        throw new NotImplementedExeption(methodName);
    }

    public static IllegalAccessError singleton(final Class<?> cls) {
        throw singleton(cls, false);
    }

    public static IllegalAccessError singleton(final Class<?> cls, final boolean simpleName) {
        final String name = simpleName ? cls.getSimpleName() : cls.getName();
        throw illegalAccess("Only one instance for " + name);
    }

    public static IllegalAccessError noInstances(final Class<?> cls) {
        throw noInstances(cls, false);
    }

    public static IllegalAccessError noInstances(final Class<?> cls, final boolean simpleName) {
        Check.nonNull(cls);
        final String className = simpleName ? cls.getSimpleName() : cls.getName();
        throw illegalAccess("No instances for " + className);
    }

    public static IllegalAccessError illegalAccess(final String message) {
        throw new IllegalAccessError(message);
    }

    public static <X> Supplier<X> lazy(Function<? super String, ? extends X> constructor, Object message) {
        return () -> constructor.apply(String.valueOf(message));
    }

    public static <X> Supplier<X> lazy(Function<? super String, ? extends X> constructor, String format, Object... args) {
        return () -> constructor.apply(String.format(format, args));
    }

    public static <X extends Throwable> X throwAlways(final Supplier<? extends X> throwable) throws X {
        throw Check.nonNull(throwable).get();
    }

    public static <X extends Throwable> void throwIf(final boolean expression, final Supplier<? extends X> throwable)
            throws X {
        if (expression) {
            throwAlways(throwable);
        }
    }

    public static <X extends Throwable> void throwUnless(final boolean expression,
                                                         final Supplier<? extends X> throwable)
            throws X {
        throwIf(!expression, throwable);
    }

    protected ExceptionHelper() {
    }

}
