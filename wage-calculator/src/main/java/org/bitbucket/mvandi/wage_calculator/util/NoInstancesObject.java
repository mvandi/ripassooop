package org.bitbucket.mvandi.wage_calculator.util;

public abstract class NoInstancesObject {

    protected NoInstancesObject() {
        throw ExceptionHelper.noInstances(getClass());
    }

}
