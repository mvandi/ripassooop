package org.bitbucket.mvandi.wage_calculator.util.time.impl;

import org.bitbucket.mvandi.wage_calculator.util.Check;
import org.bitbucket.mvandi.wage_calculator.util.time.Stopwatch;
import org.bitbucket.mvandi.wage_calculator.util.time.SystemClock;

import java.util.function.LongSupplier;

public abstract class AbstractStopwatch implements Stopwatch {

    private long startTime;
    private long stopTime;
    private long elapsedTime;

    private boolean started;
    private boolean stopped;

    protected AbstractStopwatch() {
    }

    @Override
    public final long partial() {
        checkRunning();
        return elapsedTime + elapsedTime(getCurrentTime());
    }

    @Override
    public final Stopwatch reset() {
        checkStopped();
        startTime = 0L;
        stopTime = 0L;
        elapsedTime = 0L;
        started = false;
        stopped = false;
        return this;
    }

    @Override
    public final Stopwatch restart() {
        checkStopped();
        stopped = false;
        stopTime = 0L;
        startTime = getCurrentTime();
        return this;
    }

    @Override
    public final Stopwatch start() {
        checkNotStarted();
        checkNotStopped();
        started = true;
        startTime = getCurrentTime();
        return this;
    }

    @Override
    public final long stop() {
        checkNotStopped();
        stopTime = getCurrentTime();
        stopped = true;
        elapsedTime += elapsedTime(stopTime);
        return elapsedTime;
    }

    @Override
    public final long stopAndReset() {
        final long elapsedTime = stop();
        reset();
        return elapsedTime;
    }

    @Override
    public final boolean isStarted() {
        return started;
    }

    @Override
    public final boolean isStopped() {
        return stopped;
    }

    protected LongSupplier currentTime() {
        return () -> SystemClock.currentTime(getTimeUnit());
    }

    private long getCurrentTime() {
        return currentTime().getAsLong();
    }

    private long elapsedTime(final long currentTime) {
        checkStarted();
        return currentTime - startTime;
    }

    private void checkStarted() {
        Check.state(isStarted(), "Stopwatch was not started");
    }

    private void checkNotStarted() {
        Check.state(!isStarted(), "Stopwatch was already started");
    }

    private void checkStopped() {
        Check.state(isStopped(), "Stopwatch was not stopped");
    }

    private void checkNotStopped() {
        Check.state(!isStopped(), "Stopwatch was already stopped");
    }

    private void checkRunning() {
        Check.state(isRunning(), "Stopwatch is not running");
    }

}
