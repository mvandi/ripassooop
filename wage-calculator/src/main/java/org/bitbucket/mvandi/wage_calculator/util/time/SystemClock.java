package org.bitbucket.mvandi.wage_calculator.util.time;

import org.bitbucket.mvandi.wage_calculator.util.Check;
import org.bitbucket.mvandi.wage_calculator.util.NoInstancesObject;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.*;

public class SystemClock extends NoInstancesObject {

    public static long currentTime(TimeUnit unit) {
        final long currentTime = currentTimeNanos();
        return Check.nonNull(unit) == NANOSECONDS
                ? currentTime
                : unit.convert(currentTime, NANOSECONDS);
    }

    public static long currentTimeNanos() {
        return System.nanoTime();
    }

    public static long currentTimeMicros() {
        return currentTime(MICROSECONDS);
    }

    public static long currentTimeMillis() {
        return currentTime(MILLISECONDS);
    }

    public static long currentTimeSeconds() {
        return currentTime(SECONDS);
    }

    public static long currentTimeMinutes() {
        return currentTime(MINUTES);
    }

    public static long currentTimeHours() {
        return currentTime(HOURS);
    }

    public static long currentTimeDays() {
        return currentTime(DAYS);
    }

    public static void sleep(Duration timeout) {
        final long millis = timeout.toMillis();
        final int nanos = (int) (timeout.toNanos() - NANOSECONDS.convert(millis, MILLISECONDS));
        try {
            Thread.sleep(millis, nanos);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public static void sleep(long timeout, TimeUnit unit) {
        Check.nonNull(unit, "unit is null");
        sleep(Duration.of(timeout, toTemporalUnit.get(unit)));
    }

    public static void sleepNanos(final long timeout) {
        sleep(Duration.ofNanos(timeout));
    }

    public static void sleepMicros(final long timeout) {
        sleep(timeout, MICROSECONDS);
    }

    public static void sleepMillis(final long timeout) {
        sleep(Duration.ofMillis(timeout));
    }

    public static void sleepSeconds(final long timeout) {
        sleep(Duration.ofSeconds(timeout));
    }

    public static void sleepMinutes(final long timeout) {
        sleep(Duration.ofMinutes(timeout));
    }

    public static void sleepHours(final long timeout) {
        sleep(Duration.ofHours(timeout));
    }

    public static void sleepDays(final long timeout) {
        sleep(Duration.ofDays(timeout));
    }

    protected SystemClock() {
    }

    private static final Map<TimeUnit, TemporalUnit> toTemporalUnit;

    static {
        final Map<TimeUnit, TemporalUnit> tmp = new HashMap<>();

        tmp.put(TimeUnit.NANOSECONDS, ChronoUnit.NANOS);
        tmp.put(TimeUnit.MICROSECONDS, ChronoUnit.MICROS);
        tmp.put(TimeUnit.MILLISECONDS, ChronoUnit.MILLIS);
        tmp.put(TimeUnit.SECONDS, ChronoUnit.SECONDS);
        tmp.put(TimeUnit.MINUTES, ChronoUnit.MINUTES);
        tmp.put(TimeUnit.HOURS, ChronoUnit.HOURS);
        tmp.put(TimeUnit.DAYS, ChronoUnit.DAYS);

        toTemporalUnit = Collections.unmodifiableMap(tmp);
    }

}
