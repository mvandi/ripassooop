package org.bitbucket.mvandi.wage_calculator.controller;

import org.bitbucket.mvandi.wage_calculator.view.CalculatorView;

public class SimpleCalculatorController implements CalculatorController {

    private final CalculatorView view;

    public SimpleCalculatorController(final CalculatorView view) {
        this.view = view;
    }

}
