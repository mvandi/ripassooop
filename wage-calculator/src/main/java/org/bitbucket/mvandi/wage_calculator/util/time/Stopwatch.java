package org.bitbucket.mvandi.wage_calculator.util.time;

import org.bitbucket.mvandi.wage_calculator.util.Check;
import org.bitbucket.mvandi.wage_calculator.util.time.impl.StopwatchFactoryImpl;

import java.util.concurrent.TimeUnit;

import static org.bitbucket.mvandi.wage_calculator.util.time.Helper.factory;

public interface Stopwatch {

    static Stopwatch stopwatch(final TimeUnit timeUnit) {
        return factory.unstartedStopwatch(timeUnit);
    }

    static Stopwatch stopwatch(final TimeUnit timeUnit, final boolean started) {
        return factory.stopwatch(timeUnit, started);
    }

    static Stopwatch startedStopwatch(final TimeUnit timeUnit) {
        return factory.startedStopwatch(timeUnit);
    }

    static Stopwatch unstartedStopwatch(final TimeUnit timeUnit) {
        return factory.unstartedStopwatch(timeUnit);
    }

    /**
     * Returns the {@link TimeUnit} used by this stopwatch.
     *
     * @return the {@link TimeUnit} used by this stopwatch.
     */
    TimeUnit getTimeUnit();

    /**
     * Returns the elapsed time from the start stream the stopwatch without stopping it.
     *
     * @throws IllegalStateException
     *             if the stopwatch was not started or was stopped.
     * @return the elapsed time from the start stream the stopwatch.
     */
    long partial();

    /**
     * Resets the stopwatch.
     *
     * @throws IllegalStateException
     *             if the stopwatch was not stopped.
     * @return it self.
     */
    Stopwatch reset();

    /**
     * Starts the stopwatch after being stopped.
     *
     * @throws IllegalStateException
     *             if the stopwatch was not stopped.
     * @return it self.
     */
    Stopwatch restart();

    /**
     * Starts the stopwatch.
     *
     * @throws IllegalStateException
     *             if the stopwatch was already started.
     * @return it self.
     */
    Stopwatch start();

    /**
     * Stops the stopwatch and returns the elapsed time from the start stream the stopwatch.
     *
     * @throws IllegalStateException
     *             if the stopwatch was not started or was already stopped.
     * @return the elapsed time from the start stream the stopwatch.
     */
    long stop();

    /**
     * Stops and resets the stopwatch, then returns the elapsed time from the start stream the stopwatch.
     *
     * @throws IllegalStateException
     *             if the stopwatch was not started or was already stopped.
     * @return the elapsed time from the start stream the stopwatch.
     */
    long stopAndReset();

    boolean isStarted();

    boolean isStopped();

    default boolean isRunning() {
        return isStarted() && !isStopped();
    }

}

final class Helper {

    static final StopwatchFactory factory = new StopwatchFactoryImpl();

}
