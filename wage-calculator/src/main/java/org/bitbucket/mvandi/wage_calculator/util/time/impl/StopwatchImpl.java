package org.bitbucket.mvandi.wage_calculator.util.time.impl;

import org.bitbucket.mvandi.wage_calculator.util.Check;

import java.util.concurrent.TimeUnit;

final class StopwatchImpl extends AbstractStopwatch {

    private final TimeUnit unit;

    public StopwatchImpl(final TimeUnit unit) {
        this.unit = Check.nonNull(unit, "Time unit cannot be null");
    }

    @Override
    public TimeUnit getTimeUnit() {
        return unit;
    }

}
