package org.bitbucket.mvandi.wage_calculator.util;

import java.util.function.Supplier;
import java.util.stream.Stream;

public class Check {

        public static void nonNulls(final Object first, Object... more) {
            nonNull(first);
            ArrayHelper.forEach(more, Check::nonNull);
        }

        public static void nonNullsRecursively(final Object first, Object... more) {
            nonNullRecursively(first);
            nonNullRecursively(more);
        }

        public static <T> T nonNullRecursively(final T obj) {
            nonNull(obj);
            if (obj.getClass().isArray()) {
                ArrayHelper.forEach((Object[]) obj, Check::nonNullRecursively);
            } else if (obj instanceof Iterable) {
                ((Iterable<?>) obj).forEach(Check::nonNullRecursively);
            } else if (obj instanceof Stream) {
                ((Stream<?>) obj).peek(Check::nonNullRecursively);
            }
            return obj;
        }

        public static <T> T nonNull(final T obj) {
            return nonNull(obj, "obj is null");
        }

        public static <T> T nonNull(final T obj, String message) {
            expression(obj != null, ExceptionHelper.lazy(NullPointerException::new, message));
            return obj;
        }

        public static void argument(final boolean expression) {
            argument(expression, "");
        }

        public static void argument(final boolean expression, final String message) {
            expression(expression, ExceptionHelper.lazy(IllegalArgumentException::new, message));
        }

        public static void argument(final boolean expression, final String message, final Object... args) {
            expression(expression, ExceptionHelper.lazy(IllegalArgumentException::new, message, args));
        }

        public static void state(final boolean expression) {
            state(expression, "");
        }

        public static void state(final boolean expression, final String message) {
            expression(expression, ExceptionHelper.lazy(IllegalStateException::new, message));
        }

        public static void state(final boolean expression, final String message, final Object... args) {
            expression(expression, ExceptionHelper.lazy(IllegalStateException::new, message, args));
        }

        public static <X extends Throwable> void expression(final boolean expression, Supplier<? extends X> exceptionSupplier) throws X {
            ExceptionHelper.throwUnless(expression, exceptionSupplier);
        }

        protected Check() {
        }

}
