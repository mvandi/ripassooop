package org.bitbucket.mvandi.wage_calculator.util;

public class NotImplementedExeption extends RuntimeException {

    public NotImplementedExeption(String methodName) {
        super(String.format("method '%s' is not implemented", methodName));
    }

}
