package org.bitbucket.mvandi.wage_calculator.util;

import java.util.function.Consumer;

public class ArrayHelper extends NoInstancesObject {

    public static <T> void forEach(final T[] a, final Consumer<? super T> action) {
        Check.nonNull(action);
        if (a != null) {
            for (final T e : a) {
                action.accept(e);
            }
        }
    }

    protected ArrayHelper() {
    }

}
