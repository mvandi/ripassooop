package org.bitbucket.mvandi.wage_calculator.util.time.impl;

import org.bitbucket.mvandi.wage_calculator.util.time.Stopwatch;
import org.bitbucket.mvandi.wage_calculator.util.time.StopwatchFactory;

import java.util.concurrent.TimeUnit;

public class StopwatchFactoryImpl implements StopwatchFactory {

    public StopwatchFactoryImpl() {
    }

    @Override
    public Stopwatch stopwatch(TimeUnit unit) {
        return unstartedStopwatch(unit);
    }

    @Override
    public Stopwatch stopwatch(TimeUnit unit, boolean started) {
        return started ? startedStopwatch(unit) : unstartedStopwatch(unit);
    }

    @Override
    public Stopwatch startedStopwatch(TimeUnit unit) {
        return stopwatch(unit).start();
    }

    @Override
    public Stopwatch unstartedStopwatch(TimeUnit unit) {
        return new StopwatchImpl(unit);
    }

}
