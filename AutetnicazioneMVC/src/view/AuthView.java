package view;

import model.User;

public interface AuthView {

    void displayError(String message, Object... args);

    void onLogin(User user);

    void onRegister(User user);

    void onDelete(String username);

}
