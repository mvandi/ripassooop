package view;

import controller.AuthController;
import controller.SimpleAuthController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import model.User;

import java.net.URL;
import java.util.ResourceBundle;

public class GuiPresenter implements AuthView, Initializable {

    private AuthController controller;

    @FXML
    private TextField txt_password;

    @FXML
    private TextField txt_username;

    @FXML
    void accedi(ActionEvent event) {
        final String username = txt_username.getText();
        final String password = txt_password.getText();
        controller.login(username, password);
    }

    @FXML
    void iscrivimi(ActionEvent event) {
        final String username = txt_username.getText();
        final String password = txt_password.getText();
        controller.register(username, password);
    }

    @FXML
    void cancellami(ActionEvent event) {
        final String username = txt_username.getText();
        final String password = txt_password.getText();
        controller.deleteAccount(username, password);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        controller = new SimpleAuthController(this);
    }

    @Override
    public void displayError(String message, Object... args) {
        final Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(String.format(message, args));
        alert.showAndWait();
    }

    @Override
    public void onLogin(User user) {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(String.format("'%s' ha effettuato l'accesso", user.getUsername()));
        alert.showAndWait();
    }

    @Override
    public void onRegister(User user) {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(String.format("'%s' è stato registrato", user.getUsername()));
        alert.showAndWait();
    }

    @Override
    public void onDelete(String username) {
        final Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(String.format("'%s' è stato eliminato", username));
        alert.showAndWait();
    }

}
