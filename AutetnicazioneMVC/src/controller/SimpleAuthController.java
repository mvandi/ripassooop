package controller;

import model.User;
import view.AuthView;

import java.util.*;

public class SimpleAuthController implements AuthController {

    private static final int MAX_USERS = 3;

    private final AuthView view;
    private final Map<String, User> users;

    public SimpleAuthController(AuthView view) {
        this.view = view;
        users = new HashMap<>();
    }

    @Override
    public void login(String username, String password) {
        validateUser(username, password).ifPresent(view::onLogin);
        /*
        final User user = validateUser(username, password).orElse(null);
        if (user != null) {
            view.onLogin(user);
        }
        */
    }

    @Override
    public void register(String username, String password) {
        if (username == null || username.isEmpty()) {
            view.displayError("Username vuoto");
            return;
        }

        if (password == null || password.isEmpty()) {
            view.displayError("La password è vuota");
            return;
        }

        if (users.size() >= MAX_USERS) {
            view.displayError("Limite massimo utenti (%d) raggiunto", MAX_USERS);
            return;
        }

        if (userExists(username)) {
            view.displayError("Username '%s' non disponibile", username);
            return;
        }

        final User user = new User(username, password);
        users.put(username, user);

        view.onRegister(user);
    }

    @Override
    public void deleteAccount(String username, String password) {
        validateUser(username, password)
                .ifPresent(ignore -> {
                    users.remove(username);
                    view.onDelete(username);
                });
        /*
        final User user = validateUser(username, password).orElse(null);
        if (user != null) {
            users.remove(username);
            view.onDelete(username);
        }
        */
    }

    private Optional<? extends User> validateUser(String username, String password) {
        if (!userExists(username)) {
            view.displayError("Non è stato possibile trovare un utente con username '%s'", username);
            return Optional.empty();
        }

        final User user = users.get(username);
        if (!Objects.equals(user.getPassword(), password)) {
            view.displayError("Le password non corrispondono");
            return Optional.empty();
        }
        return Optional.of(user);
    }

    private boolean userExists(String username) {
        return users.containsKey(username);
    }

}
