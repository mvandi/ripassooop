package controller;

public interface AuthController {

    void login(String username, String password);

    void register(String username, String password);

    void deleteAccount(String username, String password);

}
