package org.example.quadtree;

public interface Positionable2D {

    Vec2 getPosition();

}
