package org.example.quadtree;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

public interface QuadTree<P extends Positionable2D> {

    boolean add(P e);

    default boolean addAll(final Collection<? extends P> elements) {
        Objects.requireNonNull(elements);
        boolean success = false;
        for (final P e : elements) {
            if (add(e))
                success = true;
        }
        return success;
    }

    default boolean addAll(final P first, final P... more) {
        boolean success = add(first);
        for (final P e : more) {
            if (add(e)) {
                success = true;
            }
        }
        return success;
    }

    Rectangle getBoundary();

    List<P> get(Rectangle range);

}
