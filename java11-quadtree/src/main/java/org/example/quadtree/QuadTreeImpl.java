package org.example.quadtree;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class QuadTreeImpl<P extends Positionable2D> implements QuadTree<P> {

    public static final int DEFAULT_CAPACITY = 8;
    public static final int MAX_DEPTH = Integer.MAX_VALUE - 10;

    private final int capacity;
    private final int depth;
    private final int maxDepth;
    private final Rectangle boundary;
    private final List<P> elements;
    private final List<QuadTreeImpl<P>> children;

    public QuadTreeImpl(final Rectangle boundary) {
        this(boundary, DEFAULT_CAPACITY);
    }

    public QuadTreeImpl(final Rectangle boundary, final int capacity) {
        this(boundary, capacity, MAX_DEPTH);
    }

    public QuadTreeImpl(final Rectangle boundary, final int capacity, final int maxDepth) {
        this(boundary, capacity, 1, maxDepth);
    }

    private QuadTreeImpl(final Rectangle boundary, final int capacity, final int depth, final int maxDepth) {
        this.capacity = capacity;
        this.boundary = boundary;
        this.depth = depth;
        this.maxDepth = maxDepth;
        elements = new LinkedList<>();
        children = new LinkedList<>();
    }

    @Override
    public Rectangle getBoundary() {
        return boundary;
    }

    @Override
    public boolean add(final P e) {
        Objects.requireNonNull(e);
        if (!boundary.contains(e.getPosition())) {
            return false;
        }

        if (elements.size() < capacity) {
            return elements.add(e);
        }

        subdivide();
        for (final QuadTree<P> qt : children) {
            if (qt.add(e)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public List<P> get(final Rectangle range) {
        Objects.requireNonNull(range);
        return getAsStream(range).collect(Collectors.toCollection(LinkedList::new));
    }

    private Stream<P> getAsStream(final Rectangle range) {
        if (!boundary.intersects(range)) {
            return Stream.empty();
        }
        return Stream.concat(
                elements.stream().filter(e -> range.contains(e.getPosition())),
                children.stream().flatMap(qt -> qt.getAsStream(range)));
    }

    private void subdivide() {
        if (depth == maxDepth || !children.isEmpty()) {
            return;
        }

        final Vec2 size = boundary.getSize().div(2.0);
        final double x = boundary.getCentroid().getX();
        final double y = boundary.getCentroid().getY();
        final double dx = size.getX() / 2.0;
        final double dy = size.getY() / 2.0;

        // http://www.montereyinstitute.org/courses/DevelopmentalMath/COURSE_TEXT2_RESOURCE/U13_L1_T1_text_final_4_files/image009.jpg
        children.add(createSubTree(new Vec2(x + dx, y + dy), size));    // NW
        children.add(createSubTree(new Vec2(x + dx, y - dy), size));    // NE
        children.add(createSubTree(new Vec2(x - dx, y + dy), size));    // SW
        children.add(createSubTree(new Vec2(x - dx, y - dy), size));    // SE
    }

    private QuadTreeImpl<P> createSubTree(final Vec2 centroid, final Vec2 size) {
        final Rectangle boundary = Rectangle.createFromCentroid(centroid, size);
        return new QuadTreeImpl<>(boundary, capacity, depth + 1, maxDepth);
    }

}
