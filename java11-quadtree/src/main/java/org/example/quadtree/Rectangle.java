package org.example.quadtree;

public final class Rectangle {

    private final Vec2 centroid;
    private final Vec2 size;
    private final Vec2 topLeft;
    private final Vec2 bottomRight;

    public static Rectangle createFromCentroid(final Vec2 centroid, final Vec2 size) {
        final Vec2 halfSize = size.div(2.0);
        final Vec2 topLeft = centroid.sub(halfSize);
        final Vec2 bottomRight = centroid.add(halfSize);

        return new Rectangle(centroid, size, topLeft, bottomRight);
    }

    public static Rectangle createFromCorners(final Vec2 topLeft, final Vec2 bottomRight) {
        final Vec2 size = bottomRight.sub(topLeft);
        final Vec2 centroid = topLeft.add(size.div(2.0));

        return new Rectangle(centroid, size, topLeft, bottomRight);
    }

    public Rectangle(final Vec2 centroid, final Vec2 size, final Vec2 topLeft, final Vec2 bottomRight) {
        this.centroid = centroid;
        this.size = size;
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public Vec2 getCentroid() {
        return centroid;
    }

    public Vec2 getSize() {
        return size;
    }

    public Vec2 getTopLeft() {
        return topLeft;
    }

    public Vec2 getBottomRight() {
        return bottomRight;
    }

    public boolean contains(final Vec2 v) {
        return topLeft.getX() <= v.getX()
            && topLeft.getY() <= v.getY()
            && bottomRight.getX() >= v.getX()
            && bottomRight.getY() >= v.getY();
    }

    public boolean intersects(final Rectangle other) {
        return !(bottomRight.getX() < other.topLeft.getX()
              || bottomRight.getY() < other.topLeft.getY()
              || topLeft.getX() > other.bottomRight.getX()
              || topLeft.getY() > other.bottomRight.getY());
    }

}
