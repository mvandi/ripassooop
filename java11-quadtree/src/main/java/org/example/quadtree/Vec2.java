package org.example.quadtree;

public final class Vec2 {

    private final double x;
    private final double y;

    public static double distance(final Vec2 a, final Vec2 b) {
        return a.sub(b).magnitude();
    }

    public Vec2(final double x, final double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double direction() {
        return Math.atan2(y, x);
    }

    public double sqrMagnitude() {
        return x * x + y * y;
    }

    public double magnitude() {
        return Math.sqrt(sqrMagnitude());
    }

    public Vec2 normalized() {
        return div(magnitude());
    }

    public Vec2 neg() {
        return new Vec2(-x, -y);
    }

    public Vec2 add(final Vec2 other) {
        return new Vec2(x + other.x, y + other.y);
    }

    public Vec2 sub(final Vec2 other) {
        return new Vec2(x - other.x, y - other.y);
    }

    public Vec2 mul(final double scale) {
        return new Vec2(x * scale, y * scale);
    }

    public Vec2 div(final double scale) {
        return new Vec2(x / scale, y / scale);
    }

}
